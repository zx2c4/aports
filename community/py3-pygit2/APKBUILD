# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Pierre-Gildas MILLON <pgmillon@gmail.com>
# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=py3-pygit2
_pkgname=pygit2
pkgver=1.1.1
pkgrel=0
pkgdesc="Python bindings for libgit2"
url="https://github.com/libgit2/pygit2"
arch="all"
license="GPL-2.0 WITH GCC-exception-2.0"
depends="py3-cffi py3-six"
makedepends="python3-dev py3-setuptools libgit2-dev"
checkdepends="py3-pytest py3-hypothesis py3-cached-property"
source="https://pypi.io/packages/source/p/$_pkgname/$_pkgname-$pkgver.tar.gz
	skip-failing-test-on-s390x.patch
	fix-1.0.patch
	"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-pygit" # Backward compat
provides="py-pygit=$pkgver-r$pkgrel" # Backward compat

build() {
	python3 setup.py build_ext --inplace
}

check() {
	pytest-3
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="e57600f1eb53647106cdc703d8adcdff7742bde8d3d95d8a879275ec1ac4a4a569d6f7da68a9bf49e82f491980e19abf96e55029f3fbae52371632e6bd3b2ab3  pygit2-1.1.1.tar.gz
1bd1ed146ebfae659c20aa641f795606a32e485fab13a71db12345082a18a9de9f0129d8375ffb6bf8616c906c59d0b25fb440e1f00bcd56e3e10b9aac3363fd  skip-failing-test-on-s390x.patch
d80c2cabfe2ca074e78f53bc7f87fa3841e7bc0bc57e181e1cf72e6e728c529b5246d9cb8a15949abaa55b6c59a95ecd1c939e62121d61b3d542263793edc15d  fix-1.0.patch"
