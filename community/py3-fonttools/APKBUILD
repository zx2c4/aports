# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=py3-fonttools
_pkgname=fonttools
pkgver=4.7.0
pkgrel=0
pkgdesc="Converts OpenType and TrueType fonts to and from XML"
options="!check" # Require unpackaged 'brotli'
url="https://github.com/fonttools/fonttools"
arch="all"
license="MIT AND OFL-1.1"
depends="py3-setuptools py3-lxml py3-fs cython python3-dev"
checkdepends="py3-pytest"
subpackages="$pkgname-doc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/fonttools/fonttools/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-fonttools" # Backwards compatibility
provides="py-fonttools=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# remove interpreter line
	sed -i '1d' Lib/fontTools/mtiLib/__init__.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD/build/lib" py.test-3
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	replaces="py-$_pkgname-doc" # Backwards compatibility
	provides="py-$_pkgname-doc=$pkgver-r$pkgrel" # Backwards compatibility
	default_doc
}

sha512sums="9fc9a39c30aabd46945327d74b919a66fa0158c0e81d66ec08a4969c66b3efd0a28e68f0f64f86ef28199b3443a3c463f869d4f509a166d56ac1677c4d37cd7c  fonttools-4.7.0.tar.gz"
